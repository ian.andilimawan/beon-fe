/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: false,
    images: {
        remotePatterns: [
            {
                protocol: "http",
                hostname: "localhost",
                port: "3001",
            },
            {
                protocol: "http",
                hostname: "127.0.0.1",
                port: "8000"
            }
        ],

    }
};

export default nextConfig;
