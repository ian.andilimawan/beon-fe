
import { FormAddTransaction } from '@/modules/transaction/presentations'
import React from 'react'

type Props = {}

const AddTransaction = (props: Props) => {
  return (
    <div>
        <h1 className="uppercase font-bold text-white text-2xl">
          Add Transaction
        </h1>
        <div className='rounded-md shadow-md bg-slate-600 w-full min-h-52 mt-5'>
          <FormAddTransaction />
        </div>
    </div>
  )
}

export default AddTransaction
