import { TableTransaction } from "@/modules/transaction/presentations";
import { GetTransactionCase } from "@/modules/transaction/usecases/transaction.usecase";
import ButtonComponent from "@/shared/components/button";
import React from "react";


const Transaction = async () => {

  return (
    <div>
      <div className=" text-white">
        <TableTransaction />
      </div>
    </div>
  );
};

export default Transaction;
