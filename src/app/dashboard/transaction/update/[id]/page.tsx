import { TransactionRes } from '@/modules/transaction/models/transaction-response'
import { FormUpdateTransaction } from '@/modules/transaction/presentations'
import { GetTransactionByIdCase } from '@/modules/transaction/usecases/transaction.usecase'
import React from 'react'


const UpdateTransaction = async ({ params }: { params: { id: string } }) => {

  return (
    <div>
        <h1 className="uppercase font-bold text-white text-2xl">
          Update Transaction
        </h1>
        <div className='rounded-md shadow-md bg-slate-600 w-full min-h-52 mt-5'>
          <FormUpdateTransaction id={params.id} />
        </div>
    </div>
  )
}

export default UpdateTransaction
