import { TableOccupation } from "@/modules/occupation/presentations";
import { GetOccupationCase } from "@/modules/occupation/usecases/occupation.usecase";
import ButtonComponent from "@/shared/components/button";
import React from "react";


const Occupation = async () => {

  return (
    <div>
      <div className=" text-white">
        <TableOccupation />
      </div>
    </div>
  );
};

export default Occupation;
