import { HousingRes } from '@/modules/housing/models/housing-response'
import { FormUpdateHousing } from '@/modules/housing/presentations'
import { GetHousingByIdCase } from '@/modules/housing/usecases/housing.usecase'
import React from 'react'


const UpdateHousing = async ({ params }: { params: { id: string } }) => {

  return (
    <div>
        <h1 className="uppercase font-bold text-white text-2xl">
          Update Housing
        </h1>
        <div className='rounded-md shadow-md bg-slate-600 w-full min-h-52 mt-5'>
          <FormUpdateHousing id={params.id} />
        </div>
    </div>
  )
}

export default UpdateHousing
