
import { FormAddHousing } from '@/modules/housing/presentations'
import React from 'react'

type Props = {}

const AddHousing = (props: Props) => {
  return (
    <div>
        <h1 className="uppercase font-bold text-white text-2xl">
          Add Housing
        </h1>
        <div className='rounded-md shadow-md bg-slate-600 w-full min-h-52 mt-5'>
          <FormAddHousing />
        </div>
    </div>
  )
}

export default AddHousing
