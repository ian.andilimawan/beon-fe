import { TableHousing } from "@/modules/housing/presentations";
import { GetHousingCase } from "@/modules/housing/usecases/housing.usecase";
import ButtonComponent from "@/shared/components/button";
import React from "react";


const Housing = async () => {

  return (
    <div>
      <div className=" text-white">
        <TableHousing />
      </div>
    </div>
  );
};

export default Housing;
