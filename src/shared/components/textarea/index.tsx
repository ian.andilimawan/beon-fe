import React, { TextareaHTMLAttributes } from 'react'


const TextareaComponent = (props: TextareaHTMLAttributes<HTMLTextAreaElement>) => {
  return (
    <div>
        <textarea value={props.value} onChange={props.onChange} className='rounded-lg shadow w-full bg-white focus:outline-none text-gray-600 px-3 border border-gray-300 py-2' {...props} />
    </div>
  )
}

export default TextareaComponent
