import { HousingOccupantDetailRes } from "@/modules/housing/models/housing-response";

export interface TransactionRes {
  id: number;
  id_occupant: number;
  transaction_status?: string,
  description?: string,
  total: number,
  contribution?: string,
  type: string,
  status: string,
  monthly_fees?: Date | string | null,
  details_occupant: HousingOccupantDetailRes
}
