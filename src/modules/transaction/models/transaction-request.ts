export interface TransactionReq {
    id_occupant: string | null;
    transaction_status?: string,
    description?: string,
    total: string,
    contribution?: string,
    type: string,
    status: string,
    monthly_fees?: Date | string
    _method?: string
  }
