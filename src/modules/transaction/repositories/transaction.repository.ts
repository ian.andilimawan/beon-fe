
import { deleteDataWithToken, getDataWithToken, postDataTokenFormData, postDataWithToken } from "@utils/api"
import { DataResponse, HttpResponse, HttpResponsePaginate } from "@models/api-response.models"
import { TransactionReq } from "../models/transaction-request"
import { TransactionRes } from "../models/transaction-response"

export const AddTransactionRepository = async <T extends DataResponse>(
  data: TransactionReq
): Promise<HttpResponse<T>> => {
  const res: HttpResponse<T> = await postDataWithToken('/transaction', data)

  return res
}

export const GetTransactionRepository = async <T extends DataResponse>(
  query: string
): Promise<HttpResponsePaginate<T>> => {

  const res: HttpResponsePaginate<T> = await getDataWithToken('/transaction' + query)

  return res
}

export const GetTransactionByIdRepo = async <T extends DataResponse>(
  id: string
): Promise<HttpResponse<T>> => {
  const res: HttpResponse<T> = await getDataWithToken('/transaction/' + id)

  return res
}

export const UpdateTransactionByIdRepo = async <T extends DataResponse>(
  data: TransactionReq,
  id: string
): Promise<HttpResponse<T>> => {
  const res: HttpResponse<T> = await postDataTokenFormData('/transaction/' + id, data)

  return res
}

export const DeleteTransactionByIdRepo = async <T extends DataResponse>(
  id: string
): Promise<HttpResponse<T>> => {
  const res: HttpResponse<T> = await deleteDataWithToken('/transaction/' + id)

  return res
}

export const ReportTransactionRepo = async <T extends DataResponse>(): Promise<HttpResponse<T>> => {

  const res: HttpResponse<T> = await getDataWithToken('/report')

  return res
}
