"use client";

import ButtonComponent from "@/shared/components/button";
import InputComponent from "@/shared/components/input";
import SelectComponent from "@/shared/components/select";
import React, { FormEvent, useEffect, useState } from "react";
import { AddTransactionCase } from "../../usecases/transaction.usecase";
import toast from "react-hot-toast";
import { useRouter } from "next/navigation";
import { GetAllOccupantCase } from "@/modules/occupation/usecases/occupation.usecase";
import TextareaComponent from "@/shared/components/textarea";

type SelectType = {
  value: string,
  text: string
}

const transactionType = [
  {
    text: "Expense",
    value: "expense",
  },
  {
    text: "Income",
    value: "income",
  },
];

const contribution = [
  {
    text: "Year",
    value: "year",
  },
  {
    text: "Monthly",
    value: "monthly",
  },
];

const contributionType = [
  {
    text: "Security",
    value: "security",
  },
  {
    text: "Cleanliness",
    value: "cleanliness",
  },
];

const status = [
  {
    text: "Paid Off",
    value: "paid_off",
  },
  {
    text: "Not Paid Off",
    value: "not_paid_off",
  },
];

const FormAddTransaction = () => {
  const router = useRouter()

  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [occupant, setOccupant] = useState<SelectType[]>()
  const [isExpense, setIsExpense] = useState<boolean>(false)
  const [isYear, setIsYear] = useState<boolean>(false)


  async function onSubmit(event: FormEvent<HTMLFormElement>) {
    event.preventDefault();
    const toastId = toast.loading('Adding Data...')
    setIsLoading(true);
    const formData = new FormData(event.currentTarget);

    const id_occupant: string = formData.get("id_occupant") as string;
    const total: string = formData.get("total") as string;
    let status: string = formData.get("status") as string;
    const type: string = formData.get("type") as string;
    const transaction_status: string = formData.get("transaction_status") as string;
    const description: string = formData.get("description") as string;
    const contribution: string = formData.get("contribution") as string;
    let monthly_fees: string = formData.get("monthly_fees") as string;

    if (monthly_fees == null) {
      const currentDate = new Date();
      const formattedDate = currentDate.toISOString().slice(0, 10);
      monthly_fees = formattedDate
    }

    if (status == null || contribution == 'year') {
      status = 'paid_off'
    }

    const add = await AddTransactionCase({
      id_occupant: isExpense ? id_occupant : null,
      transaction_status,
      description,
      contribution,
      monthly_fees,
      status,
      total,
      type,
    })

    setIsLoading(false)

    if (add.valid) {
        toast.success(add.message, {
            id: toastId
        })
        router.push('/dashboard/transaction')
    } else {
        toast.error(add.message, {
            id: toastId
        })

    }

  }

  const getOccupant = async () => {
    const resp = await GetAllOccupantCase()

    if (resp.valid) {
      const select : SelectType[] = resp?.data?.map((v) => {
        return {
          value: v?.id.toString(),
          text: v?.fullname
        }
      }) ?? []

      setOccupant(select)
    }
  }

  const transactionTypeChange = async (value: string) => {
    let valBool : boolean

    if (value === 'income') {
      valBool = true
    } else {
      valBool = false
    }
    setIsExpense(valBool)
  }

  const contributionChange =  async (value: string) => {
    let valBool : boolean

    if (value !== 'year') {
      valBool = true
    } else {
      valBool = false
    }
    setIsYear(valBool)
  }

  useEffect(() => {
    getOccupant()
  }, [])

  return (
    <form
      className="w-full md:w-2/3 lg:w-1/2 p-5 space-y-5"
      onSubmit={onSubmit}
      method="POST"
      encType="multipart/form-data"
      name="add"
    >
      <div>
        <label className="text-slate-300 text-sm">Transaction Type</label>
        <SelectComponent data={transactionType} name="transaction_status" onChange={(e) => {
          transactionTypeChange(e.target.value as string)
        }} />
      </div>
      {isExpense && (
      <>
      <div>
        <label className="text-slate-300 text-sm">Occupant</label>
        <SelectComponent data={occupant as SelectType[]} name="id_occupant" />
      </div>
      <div>
        <label className="text-slate-300 text-sm">Contribution</label>
        <SelectComponent data={contribution} name="contribution" onChange={(e) => {contributionChange(e.target.value as string)}} />
      </div>
      </>
      )}
      {isExpense && (
      <div>
        <label className="text-slate-300 text-sm">Contribution Type</label>
        <SelectComponent data={contributionType} name="type" />
      </div>
      )}
      <div>
        <label className="text-slate-300 text-sm">Description</label>
        <TextareaComponent name="description" />
      </div>
      <div>
        <label className="text-slate-300 text-sm">Total</label>
        <InputComponent type="number" name="total" />
      </div>
      {isExpense && isYear && (
      <>
      <div>
        <label className="text-slate-300 text-sm">Status</label>
        <SelectComponent data={status} name="status" />
      </div>
      <div>
        <label className="text-slate-300 text-sm">Contribution Month</label>
        <InputComponent type="date" name="monthly_fees" />
      </div>
      </>
      )}
      <div className="flex space-x-3 pt-4 pb-2">
        <ButtonComponent value={"Submit"} className="px-4" type="submit" isLoading={isLoading} />
        <ButtonComponent
          value={"Cancel"}
          onClick={() => {
            router.back()
          }}
          className="px-4 bg-red-500 hover:bg-red-600 shadow"
          type="button"
        />
        <ButtonComponent
          value={"Reset"}
          className="px-4 bg-gray-500 hover:bg-gray-600 shadow"
          type="reset"
        />
      </div>
    </form>
  );
};

export default FormAddTransaction;
