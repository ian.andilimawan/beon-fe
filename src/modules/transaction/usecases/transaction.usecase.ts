import { TransactionReq } from "../models/transaction-request";
import {
  ResponseUseCase,
  ResponseUseCasePaginate,
} from "@/models/api-response.models";
import {
  AddTransactionRepository,
  DeleteTransactionByIdRepo,
  ReportTransactionRepo,
  GetTransactionByIdRepo,
  GetTransactionRepository,
  UpdateTransactionByIdRepo,
} from "../repositories/transaction.repository";
import { parseErrorArray } from "@/shared/utils/helper";
import { TransactionRes } from "../models/transaction-response";

export const AddTransactionCase = async (
  data: TransactionReq
): Promise<ResponseUseCase<null>> => {
  const add = await AddTransactionRepository(data);

  if (add.valid) {
    return {
      valid: true,
      message: "Succesfully Added Data",
      data: null,
    };
  }

  return {
    valid: false,
    message: parseErrorArray(add.errors) ?? add.message,
  };
};

export const GetTransactionCase = async (
  query?: string
): Promise<ResponseUseCasePaginate<Array<TransactionRes>>> => {
  const get = await GetTransactionRepository<Array<TransactionRes>>(query ?? "");

  if (get.valid) {
    return {
      valid: true,
      message: "Succesfully Added Data",
      data: get.data,
    };
  }

  return {
    valid: false,
    message: parseErrorArray(get.errors) ?? get.message,
  };
};

export const GetTransactionByIdCase = async (
  id: string
): Promise<ResponseUseCase<TransactionRes>> => {
  const getById = await GetTransactionByIdRepo<TransactionRes>(id);

  if (getById.valid) {
    return {
      valid: true,
      message: "Succesfully Retrive Data",
      data: getById.data,
    };
  }

  return {
    valid: false,
    message: parseErrorArray(getById.errors) ?? getById.message,
  };
};

export const UpdateTransactionByIdCase = async (
    data: TransactionReq,
    id: string
  ): Promise<ResponseUseCase<TransactionRes>> => {
    const update = await UpdateTransactionByIdRepo<TransactionRes>(data, id);

    if (update.valid) {
      return {
        valid: true,
        message: "Succesfully Update Data",
        data: null,
      };
    }

    return {
      valid: false,
      message: parseErrorArray(update.errors) ?? update.message,
    };
  };

  export const DeleteTransactionByIdCase = async (
    id: string
  ): Promise<ResponseUseCase<null>> => {
    const update = await DeleteTransactionByIdRepo(id);

    if (update.valid) {
      return {
        valid: true,
        message: "Succesfully Remove Data",
        data: null,
      };
    }

    return {
      valid: false,
      message: parseErrorArray(update.errors) ?? update.message,
    };
  };
