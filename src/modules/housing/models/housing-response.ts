export interface HousingRes {
  id: number;
  id_occupant: string;
  house_number: string;
  status: string;
  details: HousingOccupantDetailRes
}

export interface HousingOccupantDetailRes {
  id: number,
  fullname: string,
  id_card: string,
  phone_number: string,
  marriage: number,
  status: number
}
