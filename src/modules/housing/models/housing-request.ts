export interface HousingReq {
  id_occupant: string;
  house_number: string;
  status: string;
  _method?: string
}
