
import { deleteDataWithToken, getDataWithToken, postDataTokenFormData } from "@utils/api"
import { DataResponse, HttpResponse, HttpResponsePaginate } from "@models/api-response.models"
import { HousingReq } from "../models/housing-request"

export const AddHousingRepository = async <T extends DataResponse>(
  data: HousingReq
): Promise<HttpResponse<T>> => {
  const res: HttpResponse<T> = await postDataTokenFormData('/housing', data)

  return res
}

export const GetHousingRepository = async <T extends DataResponse>(
  query: string
): Promise<HttpResponsePaginate<T>> => {

  const res: HttpResponsePaginate<T> = await getDataWithToken('/housing' + query)

  return res
}

export const GetHousingByIdRepo = async <T extends DataResponse>(
  id: string
): Promise<HttpResponse<T>> => {
  const res: HttpResponse<T> = await getDataWithToken('/housing/' + id)

  return res
}

export const UpdateHousingByIdRepo = async <T extends DataResponse>(
  data: HousingReq,
  id: string
): Promise<HttpResponse<T>> => {
  const res: HttpResponse<T> = await postDataTokenFormData('/housing/' + id, data)

  return res
}

export const DeleteHousingByIdRepo = async <T extends DataResponse>(
  id: string
): Promise<HttpResponse<T>> => {
  const res: HttpResponse<T> = await deleteDataWithToken('/housing/' + id)

  return res
}
