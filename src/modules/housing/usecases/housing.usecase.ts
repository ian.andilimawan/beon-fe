import { HousingReq } from "../models/housing-request";
import {
  ResponseUseCase,
  ResponseUseCasePaginate,
} from "@/models/api-response.models";
import {
  AddHousingRepository,
  DeleteHousingByIdRepo,
  GetHousingByIdRepo,
  GetHousingRepository,
  UpdateHousingByIdRepo,
} from "../repositories/housing.repository";
import { parseErrorArray } from "@/shared/utils/helper";
import { HousingRes } from "../models/housing-response";

export const AddHousingCase = async (
  data: HousingReq
): Promise<ResponseUseCase<null>> => {
  const add = await AddHousingRepository(data);

  if (add.valid) {
    return {
      valid: true,
      message: "Succesfully Added Data",
      data: null,
    };
  }

  return {
    valid: false,
    message: parseErrorArray(add.errors) ?? add.message,
  };
};

export const GetHousingCase = async (
  query?: string
): Promise<ResponseUseCasePaginate<Array<HousingRes>>> => {
  const get = await GetHousingRepository<Array<HousingRes>>(query ?? "");

  if (get.valid) {
    return {
      valid: true,
      message: "Succesfully Added Data",
      data: get.data,
    };
  }

  return {
    valid: false,
    message: parseErrorArray(get.errors) ?? get.message,
  };
};

export const GetHousingByIdCase = async (
  id: string
): Promise<ResponseUseCase<HousingRes>> => {
  const getById = await GetHousingByIdRepo<HousingRes>(id);

  if (getById.valid) {
    return {
      valid: true,
      message: "Succesfully Retrive Data",
      data: getById.data,
    };
  }

  return {
    valid: false,
    message: parseErrorArray(getById.errors) ?? getById.message,
  };
};

export const UpdateHousingByIdCase = async (
    data: HousingReq,
    id: string
  ): Promise<ResponseUseCase<HousingRes>> => {
    const update = await UpdateHousingByIdRepo<HousingRes>(data, id);

    if (update.valid) {
      return {
        valid: true,
        message: "Succesfully Update Data",
        data: null,
      };
    }

    return {
      valid: false,
      message: parseErrorArray(update.errors) ?? update.message,
    };
  };

  export const DeleteHousingByIdCase = async (
    id: string
  ): Promise<ResponseUseCase<null>> => {
    const update = await DeleteHousingByIdRepo(id);

    if (update.valid) {
      return {
        valid: true,
        message: "Succesfully Remove Data",
        data: null,
      };
    }

    return {
      valid: false,
      message: parseErrorArray(update.errors) ?? update.message,
    };
  };
