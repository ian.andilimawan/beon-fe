
import TableHousing from "./list";
import FormAddHousing from "./add";
import FormUpdateHousing from "./update";

export { TableHousing, FormAddHousing, FormUpdateHousing };
