"use client";

import ButtonComponent from "@/shared/components/button";
import InputComponent from "@/shared/components/input";
import SelectComponent from "@/shared/components/select";
import Image from "next/image";
import React, { FormEvent, useState, useEffect } from "react";
import { HousingRes } from "../../models/housing-response";
import { UpdateHousingByIdCase, GetHousingByIdCase } from "../../usecases/housing.usecase";
import toast from "react-hot-toast";
import { useRouter } from "next/navigation";
import { GetAllOccupantCase } from "@/modules/occupation/usecases/occupation.usecase";

const status = [
  {
    text: "Inhabited",
    value: "inhabited",
  },
  {
    text: "Not Inhabited",
    value: "not_inhabited",
  },
];

type SelectType = {
  value: string,
  text: string
}

type Props = {
    id: string,
}

const FormUpdateHousing = ({id}: Props) => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [data, setData] = useState<HousingRes | null>(null);
  const [error, setError] = useState<string | null>(null);
  const [occupant, setOccupant] = useState<SelectType[]>()
  const router = useRouter()

  const STORAGE_URL: string = process.env.NEXT_PUBLIC_BASE_URL_STORAGE as "";

  async function onSubmit(event: FormEvent<HTMLFormElement>) {
    event.preventDefault();
    setIsLoading(true);
    const formData = new FormData(event.currentTarget);

    const id_occupant: string = formData.get("id_occupant") as string;
    const house_number: string = formData.get("house_number") as string;
    const status: string = formData.get("status") as string;


    const toastId = toast.loading('Update Data...')

    const update = await UpdateHousingByIdCase({
        id_occupant,
        house_number,
        status,
        _method: 'PUT'
    }, id)

    setIsLoading(false)

    if (update.valid) {

        toast.success(update.message, {
            id: toastId
        })

        router.push('/dashboard/housing')
    } else {
        toast.error(update.message, {
            id: toastId
        })

    }
  }

  const getDataById = async () => {
    const getData = await GetHousingByIdCase(id)

    if (getData.valid) {
      setData(getData.data as HousingRes)
    }
  }

  const getOccupant = async () => {
    const resp = await GetAllOccupantCase()

    if (resp.valid) {
      const select : SelectType[] = resp?.data?.map((v) => {
        return {
          value: v?.id.toString(),
          text: v?.fullname
        }
      }) ?? []

      setOccupant(select)
    }
  }

  useEffect(() => {
    getDataById()
    getOccupant()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <form
      className="w-full md:w-2/3 lg:w-1/2 p-5 space-y-5"
      onSubmit={onSubmit}
      method="POST"
      encType="multipart/form-data"
    >
      <div>
        <label className="text-slate-300 text-sm">Occupant</label>
        <SelectComponent
          data={occupant as SelectType[]}
          name="id_occupant"
          defaultValue={data?.id_occupant.toString()}
        />
      </div>
      <div>
        <label className="text-slate-300 text-sm">House Number</label>
        <InputComponent
          placeholder="House Number"
          name="house_number"
          defaultValue={data?.house_number}
        />
      </div>
      <div>
        <label className="text-slate-300 text-sm">Status</label>
        <SelectComponent
          data={status}
          name="status"
          defaultValue={data?.status}
        />
      </div>
      <div className="flex space-x-3 pt-4 pb-2">
        <ButtonComponent value={"Submit"} className="px-4" type="submit" isLoading={isLoading} />
        <ButtonComponent
          value={"Cancel"}
          onClick={() => {
            router.back()
          }}
          className="px-4 bg-red-500 hover:bg-red-600 shadow"
          type="button"
        />
        <ButtonComponent
          value={"Reset"}
          className="px-4 bg-gray-500 hover:bg-gray-600 shadow"
          type="reset"
        />
      </div>
    </form>
  );
};

export default FormUpdateHousing;
