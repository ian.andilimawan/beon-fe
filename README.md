## How to Install

1. Type in your terminal `npm install` to install the required Node.js dependencies.
2. Run `npm run dev` in your terminal to start the development server.
3. Login account email: admin@gmail.com password: password
